<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>etnShop</title>

        <spring:url value="/resources/core/css/hello.css" var="coreCss" />
        <spring:url value="/resources/core/css/bootstrap.min.css"
                    var="bootstrapCss" />
        <link href="${bootstrapCss}" rel="stylesheet" />
        <link href="${coreCss}" rel="stylesheet" />
    </head>

    <div class="container">
        <h2>Products</h2>
        
        <c:if test="${act eq 'd'}"> 
                <div class="alert alert-danger" role="alert">Row removed </div>
        </c:if>
         <c:if test="${act eq 'add'}"> 
                <div class="alert alert-success" role="alert">Row added </div>
        </c:if>
            <c:if test="${act eq 'upd'}"> 
                <div class="alert alert-success" role="alert">Row updated </div>
        </c:if>
        
            
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Serial</th>
                    <th>-</th>
                    <th>-</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${products}" var="product">
                    <tr>
                        <td>${product.id}</td>
                        <td>${product.name}</td>
                        <td>${product.serial}</td>
                        <td><a href="<c:url value='/product/show/${product.id}' />">edit</a></td>
                        <td><a href="<c:url value='/product/delete/${product.id}' />">delete</a></td>
                    </tr>	
                </c:forEach>
            </tbody>
        </table>
        <hr>
        <p>
            <a class="btn btn-primary btn-lg" href="/etnshop" role="button">Back to homepage</a>
            <a class="btn btn-primary btn-lg" href="new" role="button">New product</a>
        </p>
        <footer>
            <p>&copy; Etnetera a.s. 2015</p>
        </footer>
    </div>

    <spring:url value="/resources/core/css/bootstrap.min.js"
                var="bootstrapJs" />

    <script src="${bootstrapJs}"></script>
    <script	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</body>
</html>