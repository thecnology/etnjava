<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New/Edit Product</title>
                <spring:url value="/resources/core/css/bootstrap.min.css"
                    var="bootstrapCss" />
        <link href="${bootstrapCss}" rel="stylesheet" />
        <link href="${coreCss}" rel="stylesheet" />
    </head>
    <body>
        <div class="container">
            <h2> 

                <c:if test="${Product.id==0}"> New </c:if>
                <c:if test="${Product.id>0}"> Edit </c:if>
                    Product </h2>



            <form:form action="/etnshop/product/save" method="post" modelAttribute="Product">
                <table class="table">
                    <form:hidden path="id"/>
                    <tr>
                        <td>Name:</td>
                        <td><form:input path="name" /></td>
                    </tr>
                    <tr>
                        <td>Serial:</td>
                        <td><form:input path="serial" /></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button  class="btn btn-primary btn-lg" type="submit" >Save</button></td>
                    </tr>
                </table>
            </form:form>
        </div>
    </body>
</html>
