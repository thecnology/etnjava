package cz.etn.etnshop.controller;

import cz.etn.etnshop.dao.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cz.etn.etnshop.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping("/product")
public class ProductController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;

    @RequestMapping("/")
    public ModelAndView list(@RequestParam(required = false, defaultValue = "", value="") String act) {
        ModelAndView modelAndView = new ModelAndView("product/list");
        modelAndView.addObject("products", productService.getProducts(""));
        if (!act.equals("")) {
            modelAndView.addObject("act", act);
        }
        return modelAndView;
    }

    @RequestMapping(value = "/save", method = {RequestMethod.GET, RequestMethod.POST})
    public String addProduct(@ModelAttribute("product") Product p) {
        if (p.getId() == 0) {
            //new product, add it
            this.productService.addProduct(p);
            return "redirect:/product/?act=a";
        } else {
            //existing product, call update
            this.productService.updateProduct(p);
            return "redirect:/product/?act=u";
        }

    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public ModelAndView newProduct() {
        ModelAndView modelAndView = new ModelAndView("product/new");
        Product p = new Product();
        modelAndView.addObject("Product", p);
        return modelAndView;
    }

    @RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
    public ModelAndView show(@PathVariable("id") int id) {
        ModelAndView modelAndView = new ModelAndView("product/new");
        Product p = this.productService.getProductById(id);
        modelAndView.addObject("Product", p);
        return modelAndView;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable("id") int id) {

        boolean successDeleted = this.productService.removeProduct(id);
        if (successDeleted) {
            return "redirect:/product/?act=d";
        } else {
            return "redirect:/product/?act=nd";
        }
    }

}
