package cz.etn.etnshop.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("productDao")
public class ProductDaoImpl extends AbstractDao implements ProductDao {

    @SuppressWarnings("unchecked")
    public List<Product> getProducts(String act) {
        Criteria criteria = getSession().createCriteria(Product.class);
        return (List<Product>) criteria.list();
    }

    public void updateProduct(Product p) {
        getSession().update(p);
    }

    public void addProduct(Product p) {
        getSession().persist(p);
    }

    @Override
    public Product getProductById(int id) {
        Product p = (Product) getSession().get(Product.class, new Integer(id));

        return p;
    }

    @Override
    public boolean removeProduct(int id) {

        if (existsProduct(id)) {
            Product p = (Product) getSession().load(Product.class, new Integer(id));

            if (null != p) {
                getSession().delete(p);
                return true;
            }
            return false;
        }
        return false;

    }

    private boolean existsProduct(int id) {
        return getSession().createCriteria(Product.class)
                .add(Restrictions.eq("id", id))
                .setProjection(Projections.property("id"))
                .uniqueResult() != null;
    }

}
