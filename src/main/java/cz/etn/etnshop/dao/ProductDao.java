package cz.etn.etnshop.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

public interface ProductDao {

    @Transactional(readOnly = true)
    List<Product> getProducts(String act);

    @Transactional()
    void updateProduct(Product p);

    @Transactional()
    void addProduct(Product p);

    @Transactional()
    Product getProductById(int i);

    @Transactional()
    boolean removeProduct(int id);
}
