package cz.etn.etnshop.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import cz.etn.etnshop.dao.Product;



public interface ProductService {

    
    
    @Transactional(readOnly = true)
    List<Product> getProducts(String act);

    @Transactional
    public void addProduct(Product p);

    @Transactional
    public void updateProduct(Product p);

    @Transactional
    public Product getProductById(int i);

    @Transactional
    public boolean removeProduct(int i);
}
