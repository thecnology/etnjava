package cz.etn.etnshop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.etn.etnshop.dao.Product;
import cz.etn.etnshop.dao.ProductDao;

@Service("productService")
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    @Override
    public List<Product> getProducts(String act) {
        return productDao.getProducts(act);
    }

    @Override
    public void addProduct(Product p) {
        productDao.addProduct(p);
    }

    @Override
    public void updateProduct(Product p) {
        productDao.updateProduct(p);
    }

    @Override
    public Product getProductById(int i) {
        return productDao.getProductById(i);
    }

    @Override
    public boolean removeProduct(int i) {
        return productDao.removeProduct(i);
        
    }
}
